package adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.thien.shoestore.R;
import java.util.List;

import entities.Code;

public class CodeAdapter extends RecyclerView.Adapter<CodeAdapter.ViewHolder> {
    private List<Code> mListCode;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTxtInvoice.setText(mListCode.get(position).getInvoice());
        holder.mTxtDescription.setText(mListCode.get(position).getDescription());
        holder.mTxtPrice.setText(""+mListCode.get(position).getPrice());
        holder.mTxtStatus.setText(""+mListCode.get(position).getStatus());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView mTxtInvoice, mTxtPrice, mTxtDescription, mTxtStatus;
        private ImageView mImgCode;
        public ViewHolder(View itemView) {
            super(itemView);
            mTxtInvoice = itemView.findViewById(R.id.text_view_invoice);
            mTxtPrice = itemView.findViewById(R.id.text_view_price);
            mTxtDescription = itemView.findViewById(R.id.text_view_description);
            mTxtStatus = itemView.findViewById(R.id.text_view_status_code);
            mImgCode = itemView.findViewById(R.id.image_view_image_code);
        }
    }
}
