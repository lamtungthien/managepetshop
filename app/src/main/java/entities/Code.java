package entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
@Entity(tableName = "Code")
public class Code implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id ;//primary key
    @ColumnInfo(name = "invoice")
    private String invoice;
    @ColumnInfo(name = "status")
    private int status;
    @ColumnInfo(name = "price")
    private double price;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "date")
    private String date;
    @ColumnInfo(name = "customerName")
    private String customerName;
    @ColumnInfo(name = "phone")
    private String phone;

    public Code(int id, String invoice, int status, double price, String description, String date, String customerName, String phone) {
        this.id = id;
        this.invoice = invoice;
        this.status = status;
        this.price = price;
        this.description = description;
        this.date = date;
        this.customerName = customerName;
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
