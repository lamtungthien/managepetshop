package entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Customer")
public class Customer  {
    @PrimaryKey(autoGenerate = true)
    private int id ;
    @SerializedName("customer_name")
    @ColumnInfo(name = "name")
    private String customerName;
    @SerializedName("access_token")
    @ColumnInfo(name = "accessToken")
    private String accessToken;
    @SerializedName("phone")
    @ColumnInfo(name = "phone")
    private String phone;

    public Customer(int id, String customerName, String accessToken, String phone) {
        this.id = id;
        this.customerName = customerName;
        this.accessToken = accessToken;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
