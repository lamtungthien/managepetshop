package com.example.thien.shoestore.homepage;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.thien.shoestore.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import entities.Customer;
import serviceRepository.PetRepository;
import serviceRepository.PetRepositoryImpl;
import ultis.CallBackData;
import ultis.FacebookCallBackData;
import ultis.FacebookHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private final static int REQUEST_CODE_ACCOUNT_KIT = 99;
    Button loginButton;
    PetRepository mPetRepository;
    private CallbackManager mCallbackManager;
    Button btnLoginPhone;
    TextView txtName, txtEmail, txtFirstname;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        callbackManager = CallbackManager.Factory.create();
        printKeyHash();
        initite();
        txtName.setVisibility(View.VISIBLE);
        txtFirstname.setVisibility(View.VISIBLE);
        txtEmail.setVisibility(View.VISIBLE);
       // loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        setLogin_Button();
//        btnLoginPhone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                loginByPhoneNumber(LoginType.PHONE);
//            }
//        });
    }
    private void loginFacebook() {
        FacebookHelper.loginFacebook(this);
        mCallbackManager = CallbackManager.Factory.create();
        FacebookHelper.handleFacebookLogin(this, mCallbackManager, new FacebookCallBackData() {
            @Override
            public void onSuccess(boolean isLogged) {
                if (isLogged) {
                    mPetRepository.loginFB(LoginActivity.this, FacebookHelper.getFbAccessToken(), new CallBackData<Customer>() {
                        @Override
                        public void onSuccess(Customer customer) {
                            if(customer != null){
                                
                            }
                        }

                        @Override
                        public void onFail(String message) {

                        }
                    });
                 //   txtName.setText("Da login. AccessToken:" + FacebookHelper.getFbAccessToken());
                }

            }


            @Override
            public void onFail(String message) {
                Log.i("", "đã fail");
            }
        });


    }
    private void loginByPhoneNumber(LoginType phone) {
        Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
        startActivityForResult(intent, REQUEST_CODE_ACCOUNT_KIT);
    }


    private void setLogin_Button() {
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                result();
//                if (loginResult.getAccessToken() != null) {
//                    String accessToken = loginResult.getAccessToken().getToken();
//                    Log.d("Accesstoken", accessToken);
//                }
//                loginButton.setVisibility(View.INVISIBLE);
//                txtName.setVisibility(View.VISIBLE);
//                txtFirstname.setVisibility(View.VISIBLE);
//                txtEmail.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//
//            }
//        });
    }

    private void result() {
        final GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.d("JSON", response.getJSONObject().toString());

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email,first_name");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void initite() {
        mPetRepository = new PetRepositoryImpl();
        loginButton =  findViewById(R.id.btn_login_facebook);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtFirstname = (TextView) findViewById(R.id.txtFirstName);
        txtName = (TextView) findViewById(R.id.txtName);
        btnLoginPhone = (Button) findViewById(R.id.btn_login_phonenum);
        loginButton.setOnClickListener(this);
        btnLoginPhone.setOnClickListener(this);
    }

    private void printKeyHash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.thien.shoestore", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ACCOUNT_KIT) {
            final AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (result.getError() != null) {
                Log.e("", result.getError().getErrorType().getMessage());
                return;
            } else if (result.wasCancelled()) {

            } else {
                FacebookHelper.handleAccountKitLogin(this, result.getAccessToken().getToken(), new FacebookCallBackData() {
                    @Override
                    public void onSuccess(boolean isLogged) {
                        if (isLogged) {
                            txtName.setText("Access cuar phoen" +result.getAccessToken().getToken() +"");
                        }

                    }

                    @Override
                    public void onFail(String message) {
                        Log.e("", message);
                    }
                });

            }


        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login_facebook:
                loginFacebook();
                break;
            case R.id.btn_login_phonenum:
                loginByPhoneNumber(LoginType.PHONE);
                break;
        }
    }
}
