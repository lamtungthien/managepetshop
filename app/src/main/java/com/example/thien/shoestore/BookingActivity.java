package com.example.thien.shoestore;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BookingActivity extends AppCompatActivity {
    private static final int MAX_AVAILABLE = 1000 * 60 * 60 * 24 * 15;
    TextView tvDate;
    CarouselView carouselView;
    int[] sampleImages = {R.drawable.pet1, R.drawable.pet2, R.drawable.pet3,R.drawable.pet4,R.drawable.pet5};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        final MaterialBetterSpinner spStoreType = findViewById(R.id.spStoreType);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,new String[]{"Quận 1","Quận 2","Quận 12"});
        spStoreType.setAdapter(arrayAdapter);
        final MaterialBetterSpinner spPetType = findViewById(R.id.spPetType);
        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,new String[]{"Chó","Mèo"});
        spPetType.setAdapter(arrayAdapter1);
        final MaterialBetterSpinner spWeightPet = findViewById(R.id.spWeightPet);
        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,new String[]{"0kg-5kg","5kg-10kg","10kg-20kg"});
        spWeightPet.setAdapter(arrayAdapter2);
        final MaterialBetterSpinner spCombo = findViewById(R.id.spCombo);
        ArrayAdapter<String> arrayAdapter3 = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,new String[]{"Cắt móng","Tỉa lông","Chích thuốc","Chăm sóc Pet"});
        spCombo.setAdapter(arrayAdapter3);
        carouselView = findViewById(R.id.carsouselView);
        carouselView.setPageCount(sampleImages.length);
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(sampleImages[position]);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            }
        });
        tvDate = findViewById(R.id.edtTextDate);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate();

            }
        });
    }
    private void setDate() {
        final Calendar calendar = Calendar.getInstance();
        int date = calendar.get(Calendar.DATE);
        int mounth = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int date) {
                calendar.set(year, month, date);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                tvDate.setText(simpleDateFormat.format(calendar.getTime()));

            }
        }, year, mounth, date);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + MAX_AVAILABLE);
        datePickerDialog.show();
    }
}
