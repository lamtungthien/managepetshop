package serviceRepository;

import android.content.Context;



import java.util.List;

import entities.Code;
import entities.Customer;
import ultis.CallBackData;

public interface PetRepository {
    void loginFB(Context context, String fbAccessToken ,CallBackData<Customer> callBackData);
    void setOrder(Context context, String storeId
            ,String brandId
            ,String time
            ,String kilogram
            ,String serviceId
            ,String petId
            ,String accessToken
            , CallBackData<Code> callBackData);
}
