package serviceRepository;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import entities.Code;
import entities.Customer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ultis.CallBackData;
import ultis.ClientApi;

public class PetRepositoryImpl implements PetRepository {


    @Override
    public void loginFB(Context context,String fbAccessToken,final CallBackData callBackData) {
        ClientApi clientApi = new ClientApi();
        Call<ResponseBody> serviceCall = clientApi.storeService().loginFb(fbAccessToken);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response != null && response.body() != null){
                    if(response.code() == 200){
                        try
                        {
                            String result = response.body().string();
                            Log.e("RESULT", result);
                            Type type = new TypeToken<Customer>(){}.getType();
                            Gson gson = new Gson();
                            Customer customer = gson.fromJson(result, type);
                            callBackData.onSuccess(customer);
                        }catch (Exception e){
                            Log.d("", e.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void setOrder(Context context, String storeId, String brandId, String time, String kilogram,String serviceId,String petId,String accessToken ,final CallBackData<Code> callBackData) {
        ClientApi clientApi = new ClientApi();
        Call<ResponseBody> serviceCall = clientApi.storeService().setOrder(storeId,brandId,time,kilogram,serviceId,petId,accessToken);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response != null && response.body() != null){
                    if(response.code() == 200){
                        try
                        {
                            String result = response.body().string();
                            Log.e("RESULT", result);
                            Type type = new TypeToken<Code>(){}.getType();
                            Gson gson = new Gson();
                            Code code = gson.fromJson(result, type);
                            callBackData.onSuccess(code);
                        }catch (Exception e){
                            Log.d("", e.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
