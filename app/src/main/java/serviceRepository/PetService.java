package serviceRepository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import ultis.ConfigApi;

public interface PetService {
    @FormUrlEncoded
    @POST(ConfigApi.Api.LOGIN_FB)
    Call<ResponseBody> loginFb(@Field("fbAccessToken")String fbAccessToken);
    @FormUrlEncoded
    @POST
    Call<ResponseBody> setOrder(@Field("storeId")String storeId,
                                @Field("brandId") String brandId,
                                @Field("time") String time,
                                @Field("kilogram") String kilogram,
                                @Field("serviceId") String serviceId,
                                @Field("petId") String petId,
                                @Field("accessToken") String accessToken
    );
}
