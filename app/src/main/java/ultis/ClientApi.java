package ultis;

import serviceRepository.PetService;

public class ClientApi extends BaseApi {
    public PetService storeService(){
        return this.getService(PetService.class, ConfigApi.BASE_URL);
    }
}
