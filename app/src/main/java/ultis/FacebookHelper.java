package ultis;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

public class FacebookHelper {
    private static Profile mProfile;


    /*
     * handle and proccess for login facebook, proccess when login success, or fail
     * */
    public static void handleFacebookLogin(final AppCompatActivity context, CallbackManager callbackManager, final FacebookCallBackData callBackData) {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getAccessToken() != null) {
                    String accessToken = loginResult.getAccessToken().getToken();
                    Log.e("Accesstoken", accessToken);
                    if (isFbLoggedIn()) {
                        callBackData.onSuccess(true);
                    }
                    //get phone number

                }
            }

            @Override
            public void onCancel() {
                Log.i("", "LoginManager FacebookCallback onCancel");
                if (isFbLoggedIn()) {
                    LoginManager.getInstance().logOut();
                    callBackData.onFail("");
                }
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("", "LoginManager FacebookCallback onError");

            }
        });
    }

    public static Profile getProfile() {
        Profile profile = Profile.getCurrentProfile();
        mProfile = profile;
        return mProfile;
    }

    /*
     * process login by account kit
     * */
    public static void handleAccountKitLogin(AppCompatActivity context, String accessToken, final FacebookCallBackData callBackData) {
        if (!accessToken.isEmpty() || accessToken != null || !accessToken.equals("")) {
            Log.e("AccessToken", accessToken);
            callBackData.onSuccess(true);
        } else {
            callBackData.onFail("");
        }
    }

    /*
     * check user is logged ???
     * */
    public static boolean isFbLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    /*
     * get fb accessTOken by account kit
     * */
    public static String getFbAccessTokenByAccountKit() {
        com.facebook.accountkit.AccessToken accessToken = AccountKit.getCurrentAccessToken();
        if (accessToken != null) {
            AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                @Override
                public void onSuccess(Account account) {

                }

                @Override
                public void onError(AccountKitError accountKitError) {

                }
            });
        }
        return "";
    }

    /*
     * get accesstoken from fb
     * */
    public static String getFbAccessToken() {
        String accessToken = AccessToken.getCurrentAccessToken().getToken();
        return accessToken;
    }

    /*
     * login facebook
     * */
    public static void loginFacebook(AppCompatActivity context) {
        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Arrays.asList("public_profile, email"));
    }

    /*
     * log out fb
     * */
    public static void logOutFaceBook(AppCompatActivity context) {
        LoginManager.getInstance().logOut();
    }

}
