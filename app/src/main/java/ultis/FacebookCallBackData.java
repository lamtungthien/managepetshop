package ultis;

public interface FacebookCallBackData {
    void onSuccess(boolean isLogged);

    void onFail(String message);
}
